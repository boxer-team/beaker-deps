#!/bin/sh

# Copyright 2014 TWO SIGMA OPEN SOURCE, LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# install beaker dependencies on Debian Stretch
sudo apt-get install -y python g++ make
sudo apt-get install -y git

# java
sudo apt-get install -y openjdk-8-jre # javac -v = 1.8.XXX

# gradle
# TODO: Maybe needed...
#sudo apt-get install -y gradle

# nginx
sudo apt-get install -y nginx # nginx -v = 1.6.0

# nodejs
# TODO: patch source to not use legacy name of binary
# TODO: May need nodejs v0.12 (not v0.10)
# TODO: May need npm
sudo apt-get install -y nodejs-legacy

# for cpp
# TODO: Maybe needed...
#sudo apt-get install -y clang

# for R
# FIXME: Fails to start
sudo apt-get install -y r-base r-base-dev r-cran-rserve r-cran-ggplot2 r-cran-rcurl libxml2-dev libssl-dev libcurl4-gnutls-dev # R -v = 3.1.0
sudo Rscript -e "install.packages('devtools',,'http://cran.us.r-project.org')"
sudo Rscript -e "install.packages('RJSONIO',,'http://cran.us.r-project.org')"

# ipython
# TODO: May need ipython v3 (not v2)
sudo apt-get install -y python-yaml
sudo apt-get install -y ipython python-jinja2 python-tornado python-zmq python-pandas python-jsonschema
sudo apt-get install -y python-matplotlib python-scipy

# ipython3
# TODO: May need ipython3 v3 (not v2)
# TODO: May need python-pyaml (not python3-yaml)
# FIXME: Succeeds but throws error "no module named request" (installing python3-requests is no fix)
sudo apt-get install -y ipython3
sudo apt-get install -y python3-numpy python3-matplotlib python3-scipy python3-jinja2 python3-tornado python3-zmq python3-pandas python3-yaml #python3-pyaml

# ruby
sudo apt-get install -y ruby ruby-dev ruby-multi-json ruby-mimemagic libtool-bin autoconf libzmq3-dev
sudo gem install iruby

# julia
# FIXME: Resolve build issues for ARM: https://anonscm.debian.org/cgit/pkg-julia/julia.git/commit/debian/control?id=788453f
sudo apt-get install -y julia
julia --eval 'Pkg.add("IJulia")'
julia --eval 'Pkg.add("Gadfly")'
